api = 2
core = 7.x
projects[drupal][version] = 7.89
projects[drupal][type] = core
; performance
projects[drupal][patch][2199001] = https://www.drupal.org/files/issues/node_access_grants-static-cache-19.patch
; media
projects[drupal][patch][2211657] = https://www.drupal.org/files/issues/create-directory-on-file-copy-2211657-7.patch
; file handling
projects[drupal][patch][1399846] = https://www.drupal.org/files/issues/cleanup-files-1399846-316.patch
projects[drupal][patch][2752783] = https://www.drupal.org/files/issues/drupal-file_unmanaged_move_rename_where_possible-2752783-9.patch
; Image style previews don't show when using a private file system
projects[drupal][patch][1440312] = https://www.drupal.org/files/issues/1440312-61.patch
; missing modules notice
projects[drupal][patch][2844716] = https://www.drupal.org/files/issues/missing-modules-notice-2844716-5.patch
